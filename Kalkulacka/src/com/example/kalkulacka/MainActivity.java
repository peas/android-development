package com.example.kalkulacka;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	EditText mDisplay;
	boolean mZero=true;
	float mFirstNumber=0f;
	int mOperator=0;
	Button mButton1, mButton2, mButton3, mButton4, mButton5, mButton6, mButton7, mButton8, mButton9, mButton0, mButtonCE, mButtonEquals, mButtonAdd, mButtonSub, mButtonMul, mButtonDiv, mButtonDot;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        mDisplay = (EditText)findViewById(R.id.Result);
        mButton1 = (Button)findViewById(R.id.Button1);
        mButton2 = (Button)findViewById(R.id.Button2);
        mButton3 = (Button)findViewById(R.id.Button3);
        mButton4 = (Button)findViewById(R.id.Button4);
        mButton5 = (Button)findViewById(R.id.Button5);
        mButton6 = (Button)findViewById(R.id.Button6);
        mButton7 = (Button)findViewById(R.id.Button7);
        mButton8 = (Button)findViewById(R.id.Button8);
        mButton9 = (Button)findViewById(R.id.Button9);
        mButton0 = (Button)findViewById(R.id.Button0);
        mButtonDot = (Button)findViewById(R.id.ButtonDot);
        mButtonCE = (Button)findViewById(R.id.ButtonCE);
        mButtonEquals = (Button)findViewById(R.id.ButtonEquals);
        mButtonAdd = (Button)findViewById(R.id.ButtonAdd);
        mButtonSub = (Button)findViewById(R.id.ButtonSub);
        mButtonMul = (Button)findViewById(R.id.ButtonMul);
        mButtonDiv = (Button)findViewById(R.id.ButtonDiv);
        
        mButton1.setOnClickListener(onClickListener);
        mButton2.setOnClickListener(onClickListener);
        mButton3.setOnClickListener(onClickListener);
        mButton4.setOnClickListener(onClickListener);
        mButton5.setOnClickListener(onClickListener);
        mButton6.setOnClickListener(onClickListener);
        mButton7.setOnClickListener(onClickListener);
        mButton8.setOnClickListener(onClickListener);
        mButton9.setOnClickListener(onClickListener);
        mButton0.setOnClickListener(onClickListener);
        mButtonDot.setOnClickListener(onClickListener);
        mButtonCE.setOnClickListener(onClickListener);
        mButtonEquals.setOnClickListener(onClickListener);
        mButtonAdd.setOnClickListener(onClickListener);
        mButtonSub.setOnClickListener(onClickListener);
        mButtonMul.setOnClickListener(onClickListener);
        mButtonDiv.setOnClickListener(onClickListener);
    }
    
    private OnClickListener onClickListener = new OnClickListener(){
        public void onClick(View v) {
        	String btn;
            switch(v.getId()){
                case R.id.Button1:
                	btn="1";
                    buttonNumClicked(btn); 
                break;
                case R.id.Button2:
                	btn="2";
                    buttonNumClicked(btn); 
                break;
                case R.id.Button3:
                	btn="3";
                    buttonNumClicked(btn); 
                break;
                case R.id.Button4:
                	btn="4";
                    buttonNumClicked(btn); 
                break;
                case R.id.Button5:
                	btn="5";
                    buttonNumClicked(btn); 
                break;
                case R.id.Button6:
                	btn="6";
                    buttonNumClicked(btn); 
                break;
                case R.id.Button7:
                	btn="7";
                    buttonNumClicked(btn); 
                break;
                case R.id.Button8:
                	btn="8";
                    buttonNumClicked(btn); 
                break;
                case R.id.Button9:
                	btn="9";
                    buttonNumClicked(btn); 
                break;
                case R.id.Button0:
                	btn="0";
                    buttonNumClicked(btn); 
                break;
                case R.id.ButtonDot:
                	btn=".";
                    buttonNumClicked(btn); 
                break;
                case R.id.ButtonAdd:
                	btn="+";
                    buttonOperationClicked(btn); 
                break;
                case R.id.ButtonSub:
                	btn="-";
                    buttonOperationClicked(btn); 
                break;
                case R.id.ButtonMul:
                	btn="*";
                    buttonOperationClicked(btn); 
                break;
                case R.id.ButtonDiv:
                	btn="/";
                    buttonOperationClicked(btn); 
                break;
                case R.id.ButtonCE:
                    buttonCEClicked(); 
                break;
                case R.id.ButtonEquals:;
                    buttonEqualsClicked(); 
                break;
            }
      }
        
        public void buttonNumClicked(String btn){
        	if(mZero){
        		if(btn.equals("0"))return;
        	mDisplay.setText("");
        	mDisplay.append(btn);
        	mZero=false;
        	}
           	else
        	mDisplay.append(btn);       
        }
        
        public void buttonOperationClicked(String btn){
        	if(mOperator!=0){
        		doCalculation(mOperator);
        		setOperator(btn);
        	}
        	
        	if(mOperator==0)
        		setOperator(btn);
        }
        
        public void buttonCEClicked(){
        	mFirstNumber=0;
        	mOperator=0;
        	mDisplay.setText("0");
        	mZero=true;
        }
        
        public void buttonEqualsClicked(){
        	if(mOperator!=0)
        		doCalculation(mOperator);
        }
        
        public void setOperator(String s){
    	mFirstNumber=Float.parseFloat(mDisplay.getText().toString());
    	if(s.equals("+"))
    		mOperator=1;
    	if(s.equals("-"))
    		mOperator=2;
    	if(s.equals("*"))
    		mOperator=3;
    	if(s.equals("/"))
    		mOperator=4;
    	mZero=true;
    }
    
    public void doCalculation(int o){
    	if(mOperator==1)
    		mFirstNumber+=Float.parseFloat(mDisplay.getText().toString());
    	if(mOperator==2)
    		mFirstNumber-=Float.parseFloat(mDisplay.getText().toString());
    	if(mOperator==3)
    		mFirstNumber*=Float.parseFloat(mDisplay.getText().toString());
    	if(mOperator==4)
    		mFirstNumber/=Float.parseFloat(mDisplay.getText().toString());

    	int IsInt=Math.round(mFirstNumber);
    	if(IsInt==mFirstNumber)
    		mDisplay.setText(Integer.toString(IsInt));
    	else
    		mDisplay.setText(Float.toString(mFirstNumber));

    	mOperator=0;
    	mZero=true;
    }     
    }; 
}
